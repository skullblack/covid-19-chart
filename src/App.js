import React, {useEffect, useState} from 'react';
import axios from 'axios';
import './App.css';

function App() {
  const [dataCovid, setDataCovid] = useState([]);
  const [day, setDay] = useState(1);
  var d = new Date();

  useEffect(() => {
    const source = axios.CancelToken.source();
    axios
      .get('https://disease.sh/v3/covid-19/historical?lastdays=all', {
        cancelToken: source.token,
      })
      .then(function (response) {
        reducArrObj(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
    return () => {
      source.cancel('Component got unmounted');
    };
  }, []);

  const reducArrObj = array => {
    var result = [];
    array.reduce(function (res, value) {
      if (!res[value.country]) {
        res[value.country] = {country: value.country, cases: value.timeline.cases, backgroundColor: randomRgba()};
        result.push(res[value.country]);
      }
      return res;
    }, {});
    result.sort(function (a, b) {
      return b.cases['11/1/20'] - a.cases['11/1/20'];
    });
    setDataCovid(result);
  };

  const randomRgba = () => {
    var o = Math.round,
      r = Math.random,
      s = 255;
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
  };

  const sortData = date => {
    setDataCovid(oldData => {
      oldData.sort(function (a, b) {
        return b.cases[`11/${date}/20`] - a.cases[`11/${date}/20`];
      });
      return oldData;
    });
  };
  useEffect(() => {
    var step = false;
    const interval = setInterval(() => {
      if (dataCovid.length) {
        if (step) {
          setDay(oldDay => {
            if (oldDay == 26) {
              step = false;
              return oldDay;
            }
            sortData(oldDay);
            return oldDay + 1;
          });
        } else {
          setDay(oldDay => {
            if (oldDay == 1) {
              step = true;
              return oldDay;
            }
            sortData(oldDay);
            return oldDay - 1;
          });
        }
      }
    }, 100);
    return () => clearInterval(interval);
  }, [dataCovid]);

  // const setData = e => {
  //   setDay(e.target.value);
  //   sortData(e.target.value);
  // };

  return (
    <div className="App">
      <header className="App-header">
        <div className="title">Covid Global Cases by SGN</div>
        <div className="date">Date : 4/{day}/20</div>
        {/* <input type="number" onChange={e => setData(e)} value={day} min="1" max={d.getDate() - 1} /> */}
        <div className="chart-container" style={{position: 'relative', height: dataCovid.length * 20}}>
          {dataCovid.map((obj, i) => (
            <Graph key={i} order={i} data={obj} day={day} dataCovidFirst={dataCovid[0]} />
          ))}
        </div>
      </header>
    </div>
  );
}

export default App;

const Graph = ({order, data, day, dataCovidFirst}) => {
  return (
    <div
      className="graph"
      style={{
        position: 'absolute',
        backgroundColor: data.backgroundColor,
        top: order * 20,
        width: `${
          order == 0 ? '100%' : (data.cases[`11/${day}/20`] / dataCovidFirst.cases[`11/${day}/20`]) * 100 + '%'
        }`,
      }}>
      <div className="graph-title">{data.country + ' (' + data.cases[`11/${day}/20`] + ') cases'}</div>
    </div>
  );
};
